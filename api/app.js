var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mysql = require('mysql');
var firebase = require('firebase/app');


require('firebase/auth');
require('firebase/database');


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
const { createConnection } = require('net');



const host = 'localhost';
const user = 'root';
const pswd = 'Fan82539870';
const dbname = 'style';

var firebaseConfig = {
  apiKey: "AIzaSyA2FZwM51aQ30pWD-U9ar6EcRLu8HZy5zc",
    authDomain: "temp-ec88a.firebaseapp.com",
    databaseURL: "https://temp-ec88a.firebaseio.com",
    projectId: "temp-ec88a",
    storageBucket: "temp-ec88a.appspot.com",
    messagingSenderId: "348220230582",
    appId: "1:348220230582:web:ed534c50c71318efd94f17"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
var database = firebase.database();

const connection = mysql.createConnection({
  host: host,
  user: user,
  password: pswd,
  port: '3306',
  database: dbname
});


connection.connect(function(err) {
  console.log(err);
  if (err) console.log("connection failed");
  else console.log("connection success");
});



var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

app.get("/testApi", function(req, res, next) {
  let color = req.query.color;
  let gender = req.query.gender;
  let category = req.query.subcategory;
  let season = req.query.season;
  let query = "select * from styles where lower(gender) = " + gender + " and lower(season) = " + season + " and lower(subCategory) = " + category + " and lower(baseColour) = " + color;
  connection.query(query, function (error, results, fields) {
      if(error) throw error;
      res.send(JSON.stringify(results));
  });
});

app.get("/searchItems", function(req, res, next) {
  let keywords = req.query.keywords;
  console.log(keywords);
  let keywordsArr = keywords.split(" ");
  let subquery = "";

  let seasonExist = false;
  let season = "";
  for (let i = 0; i < keywordsArr.length; i++) {
    let curData = keywordsArr[i];
    if (curData.toLowerCase() === "spring") {
      seasonExist = true;
      season = "spring";
    } else if (curData.toLowerCase() === "summer") {
      seasonExist = true;
      season = "summer";
    } else if (curData.toLowerCase() === "fall") {
      seasonExist = true;
      season = "fall";
    } else if (curData.toLowerCase() === "winter") {
      seasonExist = true;
      season = "winter";
    }
  }
  for (let i = 0; i < keywordsArr.length; i++) {
    if (keywordsArr[i].toLowerCase() === "spring") {
      continue;
    }
    if (keywordsArr[i].toLowerCase() === "summer") {
      continue;
    }
    if (keywordsArr[i].toLowerCase() === "fall") {
      continue;
    }
    if (keywordsArr[i].toLowerCase() === "winter") {
      continue;
    }
    subquery += "%" + keywordsArr[i] + "%";
    if (i !== keywordsArr.length - 1) {
      subquery += " and ";
    }
  }

  let query = "";
  if (seasonExist && subquery.length > 0) {
    query = "select * from " + season +" where productDisplayName like " + JSON.stringify(subquery);
  } else if (seasonExist) {
    query = "select * from " + season;
  } else {
    query = "select * from styles where productDisplayName like " + JSON.stringify(subquery);
  }
  console.log(query);
  connection.query(query, function (error, results, fields) {
      if(error) throw error;
      res.send(JSON.stringify(results));
  });
});

app.get("/getImage", function(req, res, next) {
  let id = req.query.id;
  database.ref(id).on('value', function(snapshot) {
    res.send(snapshot.val());
  })
})


app.get("/searchItemsCount", function(req, res, next) {
  let keywords = req.query.keywords;
  console.log(keywords);
  let keywordsArr = keywords.split(" ");
  let subquery = "";

  let seasonExist = false;
  let season = "";
  for (let i = 0; i < keywordsArr.length; i++) {
    let curData = keywordsArr[i];
    if (curData.toLowerCase() === "spring") {
      seasonExist = true;
      season = "spring";
    } else if (curData.toLowerCase() === "summer") {
      seasonExist = true;
      season = "summer";
    } else if (curData.toLowerCase() === "fall") {
      seasonExist = true;
      season = "fall";
    } else if (curData.toLowerCase() === "winter") {
      seasonExist = true;
      season = "winter";
    }
  }
  for (let i = 0; i < keywordsArr.length; i++) {
    if (keywordsArr[i].toLowerCase() === "spring") {
      continue;
    }
    if (keywordsArr[i].toLowerCase() === "summer") {
      continue;
    }
    if (keywordsArr[i].toLowerCase() === "fall") {
      continue;
    }
    if (keywordsArr[i].toLowerCase() === "winter") {
      continue;
    }
    subquery += "%" + keywordsArr[i] + "%";
    if (i !== keywordsArr.length - 1) {
      subquery += " and ";
    }
  }

  let query = "";
  if (seasonExist && subquery.length > 0) {
    query = "select gender, count(*) from " + season +" where productDisplayName like " + JSON.stringify(subquery);
  } else if (seasonExist) {
    query = "select gender, count(*) from " + season;
  } else {
    query = "select gender, count(*) from styles where productDisplayName like " + JSON.stringify(subquery);
  }
  query += " group by gender";
  connection.query(query, function (error, results, fields) {
      if(error) throw error;
      res.send(JSON.stringify(results));
  });
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
