import React, {Component} from 'react';
import './Search.css';
import {withRouter} from "react-router-dom";
import axios from 'axios';
import Accordion from "react-bootstrap/Accordion";
import Card from "react-bootstrap/Card";
class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            gender: "",
            season: "",
            subcategory: "",
            color: "",
            images: [],
            searchRes: [],
            keywords: JSON.parse(localStorage.getItem("keywords")),
            filterShow: 1,
            menCount: 0,
            womenCount: 0
        };
        this.onChangeSearch = this.onChangeSearch.bind(this);
        this.callAndFilter = this.callAndFilter.bind(this);
    }

    onChangeSearch(e) {
        localStorage.setItem("keywords", JSON.stringify(e.target.value));
        this.setState({
            keywords: e.target.value
        })
    }


    async callAndFilter() {
        let res = await axios.get("/searchItems", {params: {
            keywords: this.state.keywords
        }});
        let searchR = res.data;
        let currentRes = searchR;
        let newRes = [];
        let tempMenCount = 0;
        let tempWomenCount = 0;

        for (let i = 0; i < currentRes.length; i++) {
            let data = currentRes[i];
            if (this.state.gender !== "" && this.state.gender !== data.gender.toLowerCase()) {
                continue;
            }
            if (this.state.season !== "" && this.state.season !== data.season.toLowerCase()) {
                continue;
            }
            if (this.state.subcategory !== "" && this.state.subcategory !== data.subCategory.toLowerCase()) {
                continue;
            }
            if (this.state.color !== "" && this.state.color !== data.baseColour.toLowerCase()) {
                continue;
            }
            if (data.gender === "Men") {
                tempMenCount += 1;
            }
            if (data.gender === "Women") {
                tempWomenCount += 1;
            }
            newRes.push(data);
        }

        let resCount = await axios.get("/searchItemsCount", {params: {
            keywords: this.state.keywords
        }});
        console.log(resCount.data);
        let resCountData = resCount.data;
        let menCount = 0;
        let womenCount = 0;
        for (let i = 0; i < resCountData.length; i++) {
            let temp = resCountData[i];
            if (temp["gender"] === "Men") {
                menCount = parseInt(temp["count(*)"]);
            }
            if (temp["gender"] === "Women") {
                womenCount = parseInt(temp["count(*)"]);
            }

        }
        console.log(menCount, womenCount);


        let allImages = [];
        for (let i = 0; i < Math.min(newRes.length,3); i++) {
            let current = newRes[i];
            let midSearch = await axios.get("/getImage", {params: {
                id: current.id
            }});
            allImages.push(midSearch.data);
        }

        if (tempMenCount === menCount && tempWomenCount === womenCount) {
            this.setState({searchRes:newRes,images: allImages, menCount: menCount, womenCount: womenCount});
        } else {
            this.setState({searchRes:newRes,images: allImages, menCount: tempMenCount, womenCount: tempWomenCount});
        }
       
    }

    async componentDidMount() {
        let res = await this.callAndFilter();
    }


    goBackBtn = (e) => {
        e.preventDefault();
        localStorage.setItem("color", JSON.stringify(""));
        localStorage.setItem("season", JSON.stringify(""));
        localStorage.setItem("keywords", JSON.stringify(""));
        localStorage.setItem("subcategory", JSON.stringify(""));
        localStorage.setItem("gender", JSON.stringify(""));
        this.props.history.push("/");
    }

    async componentDidUpdate(prevProps, prevState) {
        if (this.state.gender !== prevState.gender || this.state.color !== prevState.color || this.state.season !== prevState.season || this.state.subcategory !== prevState.subcategory) {
            let res = await this.callAndFilter();
        }
        if (this.state.filterShow !== prevState.filterShow) {
            if (this.state.filterShow === 1) {
                document.getElementById("filterSection").classList.remove("hideAnimation");
                document.getElementById("filterSection").classList.add("showAnimation");
            } else {
                document.getElementById("filterSection").classList.remove("showAnimation");
                document.getElementById("filterSection").classList.add("hideAnimation");
            }
        }
        
    }

     setCategory = (type, val) => {
        if (type === "gender") {
            if (this.state.gender === val) {
                localStorage.setItem("gender", JSON.stringify(""));
                this.setState({
                    gender: ""
                })
            } else {
                localStorage.setItem("gender", JSON.stringify(val));
                this.setState({
                    gender: val
                })
            }
        }
        if (type === "subcategory") {
            if (this.state.subcategory === val) {
                localStorage.setItem("subcategory", JSON.stringify(""));
                this.setState({
                    subcategory: ""
                })
            } else {
                localStorage.setItem("subcategory", JSON.stringify(val));
                this.setState({
                    subcategory: val
                })
            }
        }
        if (type === "season") {
            if (this.state.season === val) {
                localStorage.setItem("season", JSON.stringify(""));
                this.setState({
                    season: ""
                })
            } else {
                localStorage.setItem("season", JSON.stringify(val));
                this.setState({
                    season: val
                })
            }
        }
        if (type === "color") {
            if (this.state.color === val) {
                localStorage.setItem("color", JSON.stringify(""));
                this.setState({
                    color: ""
                })
            } else {
                localStorage.setItem("color", JSON.stringify(val));
                this.setState({
                    color: val
                })
            }
        }
    }

    hideShowFilter = (e) => {
        e.preventDefault();
        if (this.state.filterShow === 1) {
            this.setState({filterShow: 0});
        } else {
            this.setState({filterShow: 1});
        }
    }

      
    render() {
        let imagesArr = [];
        for (let i = 0; i < this.state.images.length; i++) {
            let cur = this.state.images[i];
            let curData = this.state.searchRes[i];
            console.log(curData);
            imagesArr.push(
            <div className="col-md-4 mt-3">
                 <img src={cur} key={i} alt="" className="item-image mt-3"></img>
                 <div className="mt-3">
                    <div className="helv-family product-title">{curData.productDisplayName}</div>
                    <div className="helv-family product-subtitle mt-1">{curData.usage}</div>
                    <div className="helv-family product-subtitle mt-1"><span className="season-icon">{curData.season}</span> {curData.year}</div>
                    <div className="mt-2 d-flex justify-content-start align-items-center">
                        {curData.gender === "Men" ? <div className="helv-family gender-icon d-flex justify-content-center align-items-center" style={{backgroundColor: '#1bb3ff'}}><i className="fa fa-mars"></i></div> : <div className="helv-family gender-icon d-flex justify-content-center align-items-center" style={{backgroundColor: '#FF1B8D'}}><i className="fa fa-venus"></i></div>}
                    </div>
                 </div>
            </div>
           );
        }
        return (
        <div style={{backgroundColor: 'white', minHeight: '100vh', color: 'black'}}>
                <div className="pt-3 pb-3 pl-5 pr-5 row" style={{margin: '0'}}>
                    <div className="apple-logo col-md-3" style={{textAlign:'left'}}>
                        <a className="general-family" type="button" onClick={this.goBackBtn.bind(this)} style={{fontSize: '1.5rem'}}>
                            ANASTASIA
                        </a>
                    </div>
                    <div className="col-md-5 d-flex align-items-center flex-column justify-content-center" >
                        <form style={{position: 'relative', width: '100%'}}>
                            <input className="input-style" placeholder="Search Content" onChange={this.onChangeSearch}></input>
                            <a type="button" onClick = {async () => {await this.callAndFilter()}}><i className="fa fa-search search-icon-position"></i></a>
                        </form>
                    </div>
                    <div className="col-md-4 general-family d-flex align-items-center justify-content-end">
                        <div className="general-family mr-4">Eng</div>
                        <div className="general-family mr-4">Contact</div>
                        <div className="general-family">About</div>
                    </div>
                </div>
                <div style={{backgroundColor: 'black', height: '3rem'}}>
                    <div className="container" style={{height: '100%'}}>
                        <div className="d-flex justify-content-between align-items-center" style={{height: '100%'}}>
                            <div className="ad-text" style={{color: '#dc281d'}}>New</div>
                            <div className="d-flex">
                                <div className="ad-text mr-5">Offer</div>
                                <div className="ad-text mr-5">Coupon</div>
                                <div className="ad-text">Friends</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-12 mt-3 pl-4 pr-5">
                    <div className="row align-items-center">
                            <h1 className="general-family col-md-2" style={{margin: '0'}}>Products</h1>
                            <div className="d-flex align-items-center col-md-10 justify-content-between">
                                <div className="d-flex align-items-end">
                                    <div className="helv-family"><span style={{color: '#1bb3ff'}}>Men:</span> {this.state.menCount}</div>
                                    <div className="ml-3 helv-family"><span style={{color: '#FF1B8D'}}>Women:</span> {this.state.womenCount}</div>
                                </div>
                                <a onClick={this.hideShowFilter.bind(this)} type="button" className="helv-family">{this.state.filterShow === 1 ? "Hide Filters" : "Show Filters"}<i className="ml-1 fa fa-filter"></i></a>
                            </div>
                        </div>
                    </div>
                <div className="pt-3 pb-5" style={{margin: '0', display: 'flex'}}>
                    <div className="col-md-2" id="filterSection">
                        <div className="container mt-3">
                            <Accordion defaultActiveKey="0" >
                                <hr className="mb-3"></hr>
                                <Card className="mb-2" style={{backgroundColor: 'white', border: '0'}}>
                                    <Accordion.Toggle as={Card.Header} eventKey="0" style={{backgroundColor: 'white', border: '0', padding: '0', margin: '0'}}>
                                    <h5 className="general-family text-bold">Color</h5>
                                    </Accordion.Toggle>
                                    <Accordion.Collapse eventKey="0">
                                    <Card.Body style={{ padding: '0'}}>
                                    <div className="row mt-2 mb-2 pl-2 pr-2">
                                        <a type="button" className={"col-md-3 col-sm-3  black-color "+ (this.state.color === "black" ? " selected-color" : "")} onClick={this.setCategory.bind(this, "color", "black")}></a>
                                        <a type="button" className={"col-md-3 col-sm-3  brown-color "+ (this.state.color === "brown" ? " selected-color" : "")} onClick={this.setCategory.bind(this, "color", "brown")}></a>
                                        <a type="button" className={"col-md-3 col-sm-3  blue-color "+ (this.state.color === "blue" ? " selected-color" : "")} onClick={this.setCategory.bind(this, "color", "blue")}></a>
                                        <a type="button" className={"col-md-3 col-sm-3  green-color"+ (this.state.color === "green" ? " selected-color" : "")} onClick={this.setCategory.bind(this, "color", "green")}></a>
                                        <a type="button" className={"col-md-3 col-sm-3  bronze-color mt-4"+ (this.state.color === "bronze" ? " selected-color" : "")} onClick={this.setCategory.bind(this, "color", "bronze")}></a>
                                        <a type="button" className={"col-md-3 col-sm-3  navy-blue-color mt-4"+ (this.state.color === "navy blue" ? " selected-color" : "")} onClick={this.setCategory.bind(this, "color", "navy blue")}></a>
                                        <a type="button" className={"col-md-3 col-sm-3  copper-color mt-4"+ (this.state.color === "copper" ? " selected-color" : "")} onClick={this.setCategory.bind(this, "color", "copper")}></a>
                                        <a type="button" className={"col-md-3 col-sm-3  pink-color mt-4"+ (this.state.color === "pink" ? " selected-color" : "")} onClick={this.setCategory.bind(this, "color", "pink")}></a>
                                        <a type="button" className={"col-md-3 col-sm-3  white-color mt-4"+ (this.state.color === "white" ? " selected-color" : "")} onClick={this.setCategory.bind(this, "color", "white")}></a>
                                    </div>
                                    </Card.Body>
                                    </Accordion.Collapse>
                                </Card>
                                <hr className="mb-3"></hr>
                                <Card className="mb-2" style={{backgroundColor: 'white', border: '0'}}>
                                    <Accordion.Toggle as={Card.Header} eventKey="1" style={{backgroundColor: 'white', border: '0', padding: '0', margin: '0'}}>
                                    <h5 className="general-family text-bold">Gender</h5>
                                    </Accordion.Toggle>
                                    <Accordion.Collapse eventKey="1">
                                    <Card.Body style={{ padding: '0'}}>
                                    <div className="mt-2 mb-2">
                                        <a type="button" className={"col-md-12 general-selection-box general-family" + (this.state.gender === "men" ? " selected-item" : "")} onClick={this.setCategory.bind(this, "gender", "men")}>Men</a>
                                        <a type="button" className={"col-md-12 mt-2 general-selection-box general-family" + (this.state.gender === "women" ? " selected-item" : "")} onClick={this.setCategory.bind(this, "gender", "women")}>Women</a>
                                    </div>
                                    </Card.Body>
                                    </Accordion.Collapse>
                                </Card>
                                <hr className="mb-3"></hr>
                                <Card className="mb-2" style={{backgroundColor: 'white', border: '0'}}>
                                    <Accordion.Toggle as={Card.Header} eventKey="2" style={{backgroundColor: 'white', border: '0', padding: '0', margin: '0'}}>
                                    <h5 className="general-family text-bold">Season</h5>
                                    </Accordion.Toggle>
                                    <Accordion.Collapse eventKey="2">
                                    <Card.Body style={{ padding: '0'}}>
                                    <div className="mt-2 mb-2">
                                        <a type="button" className={"col-md-12 general-selection-box general-family" + (this.state.season === "spring" ? " selected-item" : "")} onClick={this.setCategory.bind(this, "season", "spring")}>Spring</a>
                                        <a type="button" className={"col-md-12 mt-2 general-selection-box general-family" + (this.state.season === "summer" ? " selected-item" : "")} onClick={this.setCategory.bind(this, "season", "summer")}>Summer</a>
                                        <a type="button" className={"col-md-12 mt-2 general-selection-box general-family" + (this.state.season === "fall" ? " selected-item" : "")} onClick={this.setCategory.bind(this, "season", "fall")}>Fall</a>
                                        <a type="button" className={"col-md-12 mt-2 general-selection-box general-family" + (this.state.season === "winter" ? " selected-item" : "")} onClick={this.setCategory.bind(this, "season", "winter")}>Winter</a>
                                    </div>
                                    </Card.Body>
                                    </Accordion.Collapse>
                                </Card>
                                <hr className="mb-3"></hr>
                                <Card className="mb-2" style={{backgroundColor: 'white', border: '0'}}>
                                    <Accordion.Toggle as={Card.Header} eventKey="3" style={{backgroundColor: 'white', border: '0', padding: '0', margin: '0'}}>
                                    <h5 className="general-family text-bold">Category</h5>
                                    </Accordion.Toggle>
                                    <Accordion.Collapse eventKey="3">
                                    <Card.Body style={{ padding: '0'}}>
                                    <div className="mt-2 mb-2">
                                        <a type="button" className={"col-md-12 general-selection-box general-family" + (this.state.subcategory === "watches" ? " selected-item" : "")} onClick={this.setCategory.bind(this, "subcategory", "watches")}>Watches</a>
                                        <a type="button" className={"col-md-12 mt-2 general-selection-box general-family" + (this.state.subcategory === "socks" ? " selected-item" : "")} onClick={this.setCategory.bind(this, "subcategory", "socks")}>Socks</a>
                                        <a type="button" className={"col-md-12 mt-2 general-selection-box general-family" + (this.state.subcategory === "topwear" ? " selected-item" : "")} onClick={this.setCategory.bind(this, "subcategory", "topwear")}>Topwear</a>
                                        <a type="button" className={"col-md-12 mt-2 general-selection-box general-family" + (this.state.subcategory === "innerwear" ? " selected-item" : "")} onClick={this.setCategory.bind(this, "subcategory", "innerwear")}>Innerwear</a>
                                        <a type="button" className={"col-md-12 mt-2 general-selection-box general-family" + (this.state.subcategory === "bottomwear" ? " selected-item" : "")} onClick={this.setCategory.bind(this, "subcategory", "bottomwear")}>Bottomwear</a>
                                        <a type="button" className={"col-md-12 mt-2 general-selection-box general-family" + (this.state.subcategory === "bags" ? " selected-item" : "")} onClick={this.setCategory.bind(this, "subcategory", "bags")}>Bags</a>
                                    </div>
                                    </Card.Body>
                                    </Accordion.Collapse>
                                </Card>
                                <hr className=""></hr>
                            </Accordion>
                        </div>
                    </div>
                    <div className={"transition-time " + (this.state.filterShow === 1 ? "col-md-10" : "col-md-12")}>
                        <div className="row pl-3 pr-5">
                            {imagesArr.length > 0 ? imagesArr : <div className="general-family">No items are found in the database</div>}
                        </div>
                    </div>
                </div>
                {/* <div>{this.state.gender}</div>
                <div>{this.state.season}</div>
                <div>{this.state.color}</div>
                <div>{this.state.subcategory}</div> */}
        </div>)
    }
}

export default withRouter(Search);