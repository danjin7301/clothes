import React, {Component} from 'react';
import './Home.css';
import Nav from '../Nav/Nav';
import Modal from "react-bootstrap/Modal";
import {withRouter} from "react-router-dom";
class Home extends Component {
    constructor(props) {
        super(props);
        localStorage.setItem("keywords", ""); 
        this.state = {
            keywords: ""
        };
        this.onChangeKeys = this.onChangeKeys.bind(this);
      }


    onChangeKeys(e) {
        localStorage.setItem("keywords", JSON.stringify(e.target.value));
        this.setState({
            keywords: e.target.value
        })
    }

    submitClick = (e) => {
        e.preventDefault();
        if (this.state.keywords.length === 0) {
            alert("Please type some words");
        } else {
            this.props.history.push("/search");
        }
    }

    render() {
        return (
            <div>
                <section className="top-layout">
                    <Nav></Nav>
                    <div>
                        <div style={{fontSize:'0.9rem'}}>NEW SEASON COLLECTION 2020</div>
                        <h1 className="front-big-title">Keep it Cool</h1>
                    </div>
                </section>
                <section style={{paddingTop: '6rem'}}>
                    <div className="container mt-5">
                        <div className="row">
                            <div className="col-md-6">
                                <img alt="" src={require('../../images/cloth1.jpg')} style={{width: '100%', marginBottom: '6rem'}}></img>
                                <div className="subsub-text">Aesthetic Design</div>
                                <div className="subsubsub-text" style={{marginTop: '1rem'}}>Grab the tail of this summer</div>
                                <div style={{display: 'flex', justifyContent: 'center', marginTop: '1rem'}}><button className="subsub-button">Shop Now</button></div>
                            </div>
                            {/* <div className="col-md-2"></div> */}
                            <div className="col-md-6">
                                <div className="subsub-text" style ={{marginTop: '6rem'}}>Minimal Looks</div>
                                <div className="subsubsub-text" style={{marginTop: '1rem'}}>How to wear fashion</div>
                                <div style={{display: 'flex', justifyContent: 'center', marginTop: '1rem'}}><button className="subsub-button">Shop Now</button></div>
                                <img alt="" className="" src={require('../../images/cloth2.jpg')} style={{width: '100%', marginTop: '6rem'}}></img>
                            </div>
                        </div>
                    </div>
                </section>

                <section style={{paddingTop: '10rem', paddingBottom: '5rem'}}>
                    <h1 className="subtitle-text">
                        All Collections
                    </h1>
                    <div className="container mt-5">
                        <div className="row">
                            <div className="col-md-4" style={{paddingLeft: '2rem', paddingRight: '2rem'}}>
                                <img alt="" src={require('../../images/ring.jpg')} style={{width: '100%'}}></img>
                                <div className="collection-text mt-3">Golden Rings</div>
                                <div className="subsubsub-text mt-2"></div>
                            </div>
                            <div className="col-md-4" style={{marginTop: '4rem', paddingLeft: '2rem', paddingRight: '2rem'}}>
                              
                                <img alt="" className="" src={require('../../images/pants.jpg')} style={{width: '100%'}}></img>
                                <div className="collection-text mt-3">Pants</div>
                                <div className="subsubsub-text mt-2"></div>
                            </div>
                            <div className="col-md-4" style={{paddingLeft: '2rem', paddingRight: '2rem'}}>
                              
                                <img alt="" className="" src={require('../../images/cloth3.jpg')} style={{width: '100%'}}></img>
                                <div className="collection-text mt-3">Clothes</div>
                                <div className="subsubsub-text mt-2"></div>
                            </div>
                        </div>
                    </div>
                </section>


                <section style={{paddingTop: '5rem', backgroundColor: "#f4f4f4", paddingBottom: '5rem'}}>
                    <div className="mt-5 container mb-5">
                        <div className="row" style={{margin: '0'}}>
                            <div className="col-md-6">
                                <img alt="" src={require('../../images/fashion.jpg')} style={{width: '100%', float:'right'}}></img>
                            </div>
                            <div className="col-md-2"></div>
                            <div className="col-md-4" style={{paddingLeft: '2rem', paddingRight: '2rem'}}>
                                <div className="subsub-text">Brand</div>
                                <div className="subsubsub-text" style={{marginTop: '1rem', lineHeight: '1.4'}}>Our company has been providing the best services for our customers. We care about clothes, fashion and every one of you who buy our products.</div>
                                <img alt="" className="mt-5" src={require('../../images/front4.jpg')} style={{width: '100%'}}></img>
                                <div className="subsub-text mt-5" style={{ fontSize: '1.4rem'}}>Follow us on social media</div>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="d-flex justify-content-center flex-column align-items-center" style={{paddingTop: '7rem', paddingBottom: '7rem'}}>
                    <div className="subsub-text">We are ANASTASIA</div>
                    <div className="container mt-5">
                        <div className="row" style={{justifyContent: 'center'}}>
                            <div className="col-md-6" style={{justifyContent: 'flex-end', display: 'flex'}}><button type="button" className="service-button">Explore Our Collection</button></div>
                            <div className="col-md-6"><button className="service-button">Recommended Items</button></div>
                        </div>
                    </div>
                    
                   
                </section>

                <section style={{paddingTop: '4rem', paddingBottom: '4rem'}}>
                    {/* <div className="subsub-text">We are ANASTASIA</div> */}
                    <div className="container">
                        <hr className="mb-2"></hr>
                        <div className="row" style={{justifyContent: 'center'}}>
                            <div className="col-md-3 mt-3">
                                <div className="footer-title">Shop</div>
                                <div className="mt-4 row footer-layout">
                                    <div className="col-md-4">
                                        SWIM
                                    </div>
                                    <div className="col-md-4">
                                        BASE
                                    </div>
                                    <div className="col-md-4">
                                        UNDERWEAR
                                    </div>
                                </div>
                                <div className="mt-3 row footer-layout">
                                    <div className="col-md-4">
                                        NEW
                                    </div>
                                    <div className="col-md-4">
                                        CONTACT
                                    </div>
                                    <div className="col-md-4">
                                        SERVICES
                                    </div>
                                </div>

                            </div>
                            <div className="col-md-1 mt-3"></div>
                            <div className="col-md-5 mt-3">
                            <div className="footer-title">Become a subscriber</div>
                                <form className="mt-4 row" style={{alignItems:'center'}}>
                                    <input className="input-subscribe general-family" ></input>
                                    <button className="footer-smalltitle" placeholder="Email">Subscribe</button>
                                </form>

                            </div>
                            <div className="col-md-3 mt-3">
                                <div className="footer-title">Follow Us</div>
                                <div className="mt-4">
                                <div className="mt-4 row footer-layout">
                                    <div className="col-md-4">
                                        Instagram
                                    </div>
                                    <div className="col-md-4">
                                        SnapChat
                                    </div>
                                    <div className="col-md-4">
                                        Facebook
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

export default withRouter(Home);